MCS = mcs

MCSFLAGS = -pkg:dotnet -lib:/usr/lib/mono/4.5 -unsafe

SRCDIR  = src/*.cs src/BinaryTree/*.cs src/Data/*.cs src/Huffman/*.cs
BUILDDIR = build/project.out

.PHONY: build clean

build:
	mkdir -p build
	$(MCS) $(MCSFLAGS) $(SRCDIR) -out:$(BUILDDIR)

clean:
	@rm -f build/*

