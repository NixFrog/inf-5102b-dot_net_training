# INF-5102B Plateform DotNet
## Content
This is a student project made to discover .Net technology and the C# language.

I implemented a basic, generic tree structure made for huffman coding.

## Build
On linux
```
make clean && make
```