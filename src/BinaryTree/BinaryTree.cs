namespace TP2
{
	public class BinaryTree<T> where T : System.IComparable 
	{
		public BinaryTreeNode<T> Root { get; set; }

		public BinaryTree()
		{
			Root = null;
		}

		public BinaryTree(BinaryTreeNode<T> root)
		{
			Root = root;
		}

		public void Clear()
		{
			Root = null;
		}

		public override string ToString()
		{
			return Root.ToString();
		}
	}
}
