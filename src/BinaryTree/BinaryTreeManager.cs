namespace TP2
{
	public static class BinaryTreeManager<T>
		where T : System.IComparable
				, IMergeable<T>
	{
		public static BinaryTree<T> CreateBinaryTree(NodeList<T, BinaryTreeNode<T>> nodeList)
		{
			BinaryTreeNode<T> min1, min2;
			var fusedNode = new BinaryTreeNode<T>();

			while(nodeList.Count > 2){
				FindRemoveMins(nodeList, out min1, out min2);
				fusedNode = new BinaryTreeNode<T>((min1.Data.Merge(min2.Data)), min1, min2);
				nodeList.Add(fusedNode);
			}

			if(nodeList.Count == 2){
				var node1 = nodeList[1];
				var node0 = nodeList[0];
				fusedNode = new BinaryTreeNode<T>((node0.Data.Merge(node1.Data)), node0, node1);
			}

			return new BinaryTree<T>(fusedNode);
		}

		private static void FindRemoveMins(NodeList<T, BinaryTreeNode<T>> nodeList, out BinaryTreeNode<T> min1, out BinaryTreeNode<T> min2)
		{
			var tmp1 = nodeList[0];
			var tmp2 = nodeList[0];
			int i1 = 0, i2 = 0;

			for(int i = 0; i < nodeList.Count; i++)
			{
				if(nodeList[i].CompareTo(tmp2) <= 0){
					if(nodeList[i].CompareTo(tmp1) <= 0){
						tmp2 = tmp1;
						tmp1 = nodeList[i];
						i2 = i1;
						i1 = i;
					}
					else{
						tmp2 = nodeList[i];
						i2 = i;
					}
				}
			}

			min1 = tmp1;
			min2 = tmp2;

			if(i1 == i2)
			{
				nodeList.RemoveAt(i1);
			}
			else if(i1 > i2){
				nodeList.RemoveAt(i1);
				nodeList.RemoveAt(i2);
			}
			else{
				nodeList.RemoveAt(i2);
				nodeList.RemoveAt(i1);
			}
		}
	}
}
