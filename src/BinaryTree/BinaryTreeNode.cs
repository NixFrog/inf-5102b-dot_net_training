namespace TP2
{
	public class BinaryTreeNode<T>
		: Node<T, BinaryTreeNode<T>>
		where T : System.IComparable
	{
		public BinaryTreeNode() : base() {}
		public BinaryTreeNode(T data) : base(data) {}
		public BinaryTreeNode(T data, BinaryTreeNode<T> left, BinaryTreeNode<T> right)
		{
			Data = data;
			var children = new NodeList<T, BinaryTreeNode<T>>(2);
			children[0] = left;
			children[1] = right;
			Neighbours = children;
		}

		public BinaryTreeNode<T> Left
		{
			get { return sideGetter(0); }
			set { sideSetter(0, value); }
		}

		public BinaryTreeNode<T> Right
		{
			get { return sideGetter(1); }
			set { sideSetter(1, value); }
		}

		private BinaryTreeNode<T> sideGetter(int sideIndex)
		{
			if(base.Neighbours == null){
				return null;
			}
			else{
				return (BinaryTreeNode<T>) base.Neighbours[1];
			}
		}

		private void sideSetter(int sideIndex, BinaryTreeNode<T> value)
		{
			if (base.Neighbours == null){
				base.Neighbours = new NodeList<T, BinaryTreeNode<T>>(2);
			}
			base.Neighbours[sideIndex] = value;
		}
	}
}
