namespace TP2
{
	public interface INode<T, U>
		: System.IComparable
		where U : INode<T, U>
	{
		T Data {get; set;}
		NodeList<T, U> Neighbours {get; set;}
	}
}
