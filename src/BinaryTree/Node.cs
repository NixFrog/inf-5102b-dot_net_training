namespace TP2
{
	public class Node<T, U>
		: INode<T, U>
		where T : System.IComparable
		where U : INode<T, U>
	{
		public T Data {get; set;}
		public NodeList<T, U> Neighbours {get; set;}
		public bool[] Code {get; set;} = new bool[0];

		public Node() 
		{
			Neighbours = new NodeList<T, U>();
		}

		public Node(T data)
		{
			Data = data;
			Neighbours = new NodeList<T, U>();
		}

		public Node(T data, NodeList<T, U> neighbours)
		{
			Data = data;
			Neighbours = neighbours;
		}

		public int CompareTo(System.Object obj)
		{
			if(obj == null){
			}

			var p = (U)obj;
			if((System.Object)p == null){
			}

			return Data.CompareTo(p.Data);
		}

		public override string ToString()
		{
			string str = Data.ToString();
			str += "[";
			foreach(var b in Code){
				if(b){str += "0";}
				else{ str += "1";}
			}
			str += "]->{";
			str += Neighbours.ToString();
			str += "} ";
			return str;
		}
	}
}
