using System.Collections.ObjectModel;

namespace TP2
{
	public class NodeList<T, U>
		: Collection<U>
		where U : INode<T, U>
	{
		public NodeList(): base(){}
		public NodeList(int initialSize)
		{
			for(int i = 0; i < initialSize; ++i){
				base.Items.Add(default(U));
			}
		}

		public U FindByValue(T value)
		{
			foreach(var node in Items){
				if(node.Data.Equals(value)){
					return node;
				}
			}
			return default(U);
		}

		public override string ToString()
		{
			string str = "";
			foreach(var node in Items){
				str += node.ToString() + "; ";
			}
			return str;
		}
	}
}
