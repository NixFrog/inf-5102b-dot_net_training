using System.Collections.Generic;

namespace TP2
{
	public static class FrequencyCounter
	{
		public static Dictionary<char, int> ComputeFrequencyTable(string input)
		{
			var table = new Dictionary<char, int>();
			
			foreach(var character in input){
				if(!table.ContainsKey(character)){
					table.Add(character, 1);
				}
				else{
					table[character] += 1;
				}
			}
			
			return table;
		}
	}
}
