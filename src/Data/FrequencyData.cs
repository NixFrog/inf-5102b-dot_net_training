namespace TP2
{
	public class FrequencyData
		: System.IComparable
		, IMergeable<FrequencyData>
	{
		public char Character {get;}
		public int Frequency {get; set;}

		public FrequencyData(char character, int frequency)
		{
			Character = character;
			Frequency = frequency;
		}

		public int CompareTo(System.Object obj)
		{
			int comparisonResult = 0;
			
			if(obj == null){
				throw new System.ArgumentException("Object must be non null");
			}

			var fdata = obj as FrequencyData;
			if((System.Object)fdata == null){
				throw new System.ArgumentException("Object must be of type FrequencyData");
			}

			try{
				comparisonResult = Frequency.CompareTo(fdata.Frequency);
			}
			catch(System.Exception e){
				System.Console.WriteLine("Error in comparisonResult" + e);
			}

			return comparisonResult;
		}

		public FrequencyData Merge(FrequencyData target)
		{
			var character = Character;
			var frequency = Frequency + target.Frequency;
			return new FrequencyData(character, frequency);
		}

		public override string ToString()
		{
			string str = "(" + Character.ToString();
			str += ", " + Frequency +")";
			return str;
		}
	}
}
