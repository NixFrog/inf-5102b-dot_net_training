namespace TP2
{
	public interface IMergeable<T>
	{
		T Merge(T target);
	}
}
