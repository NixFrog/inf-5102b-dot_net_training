using System.Collections.Generic;

namespace TP2
{
	static class HuffmanCoder<T> where T: System.IComparable
	{
		public static Dictionary<T, bool[]> TCodeDic {get; set;}

		public static void Encode(ref BinaryTree<T> bTree)
		{
			TCodeDic = new Dictionary<T, bool[]>();

			bTree.Root.Code = new bool[]{true};
			var root = bTree.Root;
			EncodeNeighbours(ref root);
			bTree.Root = root;
		}

		public static void EncodeNeighbours(ref BinaryTreeNode<T> node)
		{
			var parentCode = node.Code;
			bool[] nodeCodeR = new bool[parentCode.Length + 1];
			bool[] nodeCodeL = new bool[parentCode.Length + 1];
			System.Array.Copy(parentCode, nodeCodeL, parentCode.Length);
			System.Array.Copy(parentCode, nodeCodeR, parentCode.Length);
			var neighbourCount = node.Neighbours.Count;

			if(node.Neighbours.Count == 0){
				TCodeDic.Add(node.Data, node.Code);
			}
			else{
				nodeCodeL[parentCode.Length] = true;
				var left = node.Neighbours[0];
				left.Code = nodeCodeL;
				EncodeNeighbours(ref left);
				node.Neighbours[0] = left;

				if(neighbourCount == 2){
					nodeCodeR[parentCode.Length] = false;
					var right = node.Neighbours[1];
					right.Code = nodeCodeR;
					EncodeNeighbours(ref right);
					node.Neighbours[1] = right;
				}
			}
		}
	}
}
