﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace TP2
{
    static class Program
    {
        static void Main()
        {
			var input = System.IO.File.ReadAllText("./resources/input.txt");
			var cleanInput = input.Remove(input.Length - 1);
			var frequencies = FrequencyCounter.ComputeFrequencyTable(cleanInput);

			System.Console.WriteLine("\n------------- FREQUENCIES ---------------");
			var lines = frequencies.Select(kvp => kvp.Key + ": " + kvp.Value.ToString());
			var frequency_string= string.Join(Environment.NewLine, lines);
			System.Console.WriteLine(frequency_string);

			System.Console.WriteLine("\n------------- NODE LIST ---------------");
			var nodeList = new NodeList<FrequencyData, BinaryTreeNode<FrequencyData>>();

			foreach(var entry in frequencies){
				var data = new FrequencyData(entry.Key, entry.Value);
				var node = new BinaryTreeNode<FrequencyData>(data);
				nodeList.Add(node);
			}
			System.Console.WriteLine(nodeList.ToString());

			System.Console.WriteLine("\n---------------- TREE ------------------");
			var binaryTree = BinaryTreeManager<FrequencyData>.CreateBinaryTree(nodeList);
			System.Console.WriteLine(binaryTree.ToString());

			System.Console.WriteLine("\n------------ BINARY TREE ---------------");
			HuffmanCoder<FrequencyData>.Encode(ref binaryTree);
			System.Console.WriteLine(binaryTree.ToString());

			System.Console.WriteLine("\n------------ HUFFMAN CODE ---------------");
			foreach(var val in HuffmanCoder<FrequencyData>.TCodeDic){
				string str = val.Key + " ";
				foreach(var b in val.Value){
					if(b){str+="0";}
					else{ str+="1";}
				}
				System.Console.WriteLine(str);
			}

			//System.Console.ReadKey();
        }
    }
}
